package org.aliciafdezrov.Fibonacci;

/**
 * Describe the class
 * 
 * @author Alicia Fernández Rovira
 *
 */
public class Fibonacci{
	
	public int compute(int val){
		int ans = 0;
		if(val == 1 || val == 2) {
			ans = 1;
		}else if(val == 3){
			ans = 2;
		}else if(val < 0){
			throw new RuntimeException("Error: Valor negativo no aceptado (" + val+ ")");
		}else{
			ans = compute(val-1) + compute(val-2);
		}
		return ans; 
	}
}
