package org.aliciafdezrov.Fibonacci;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
/**
 * Created by aliciafdezrov 
 */
public class FibonacciTest{
	
	private Fibonacci fib;
	
	@Before
	public void startup(){
		fib = new Fibonacci(); 
	}
	
	@After 
	
	public void shutdown(){
		fib = null;
	}

	
	@Test
	public void shouldFibonacciOfZeroReturnZero(){
		int expectedValue = 0;
		assertEquals(expectedValue, fib.compute(0));
	}
	
	@Test 
	public void shouldFibonacciOfOneReturnOne(){
		int expectedValue = 1;
		assertEquals(expectedValue, fib.compute(1));
	}
	
	@Test 
	public void shouldFibonacciOfTwoReturnOne(){
		int expectedValue = 1;
		assertEquals(expectedValue, fib.compute(2));
	}
	
	@Test 
	public void shouldFibonacciOfThreeReturnTwo(){
		int expectedValue = 2;
		assertEquals(expectedValue, fib.compute(3));
	}
	
	@Test 
	public void shouldFibonacciOfFourReturnThree(){
		int expectedValue = 3;
		assertEquals(expectedValue, fib.compute(4));
	}
	
	@Test 
	public void shouldFibonacciOfFiveReturnFive(){
		int expectedValue = 5;
		assertEquals(expectedValue, fib.compute(5));
	}
	
	@Test 
	public void shouldFibonacciOfSixReturnEight(){
		int expectedValue = 8;
		assertEquals(expectedValue, fib.compute(6));
	}
 
}
